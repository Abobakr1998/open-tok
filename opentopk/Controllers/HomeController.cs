﻿using Microsoft.AspNetCore.Mvc;
using OpenTokSDK;
namespace opentopk.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            int ApiKey = 47695071; // YOUR API KEY
            string ApiSecret = "f9d6b00c07035fdba57adaad06c0f55cb7bc0bbc";

            var OpenTok = new OpenTok(ApiKey, ApiSecret);
            var session = OpenTok.CreateSession();
            //string sessionId = session.Id;
            string sessionId = "1_MX40NzY5NTA3MX5-MTY4MTAzMzMxMTc0MH5iZTRwZ3VQWUU5M21hU2RSaTdLWmRPaEF-UH5";

            var token = OpenTok.GenerateToken(sessionId);


            ViewBag.ApiKey = ApiKey;
            ViewBag.SessionId = sessionId.ToString();
            ViewBag.Token = token;
            //SignalProperties signalProperties = new SignalProperties("data", "type");
            //OpenTok.Signal(sessionId, signalProperties);

            //string connectionId = "CONNECTIONID";
            //OpenTok.Signal(sessionId, signalProperties, connectionId);

            return View();
        }

    }
}
